package com.project.smarthome.smarthome.Code;

public final class Constants {

    private Constants() {
    }

    // Config
    public static final boolean MESSAGE_QUEUES_SWITCHED_ON = false;
    public static final boolean USER_API_SWITCHED_ON = false;


    // Temperatures
    public static final int TEMPERATURE_MIN = 9;
    public static final int TEMPERATURE_MAX = 30;

    public static final String HOME_GEOFENCE_ID = "home";
}
