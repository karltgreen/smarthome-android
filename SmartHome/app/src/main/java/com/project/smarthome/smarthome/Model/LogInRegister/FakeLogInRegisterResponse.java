package com.project.smarthome.smarthome.Model.LogInRegister;

import com.project.smarthome.smarthome.Model.Devices.Lighting.CustomLightColour;
import com.project.smarthome.smarthome.Model.Devices.Sensors.SensorDevice;
import com.project.smarthome.smarthome.Model.HouseConfiguration;
import com.project.smarthome.smarthome.Model.Preferences.Heating.HeatingPreference;
import com.project.smarthome.smarthome.Model.Preferences.Lighting.LightingPreference;
import com.project.smarthome.smarthome.Model.Preferences.UserPreference;

import java.util.ArrayList;

public class FakeLogInRegisterResponse {

    public static LogInRegisterResponse login() {
        HouseConfiguration houseConfig = new HouseConfiguration();
        houseConfig.setHouseId("houseID123");

        ArrayList<SensorDevice> sensors = new ArrayList<>();
        sensors.add(new SensorDevice(1, "motionSensor", "onChanged", 0));
        sensors.add(new SensorDevice(2, "lightSensor", "average", 1));
        sensors.add(new SensorDevice(1, "touchSensor", "onChanged", 2));
        houseConfig.setSensors(sensors);

        UserPreference userPref = new UserPreference();
        userPref.setFirstName("Karl");
        userPref.setLastName("Green");
        userPref.setUserId(1);
        userPref.setPriority(1);
        userPref.heatingPreference = new HeatingPreference();
        userPref.heatingPreference.setTargetTemp(18);
        userPref.heatingPreference.setAutomationType("location");
        userPref.lightingPreference = new LightingPreference();
        userPref.lightingPreference.setBrightness(200);
        userPref.lightingPreference.setSaturation(200);
        userPref.lightingPreference.setAutomationType("motion");
        userPref.lightingPreference.setColour(new CustomLightColour(100, 30, 200));

        return new LogInRegisterResponse(houseConfig, userPref);
    }

    public static LogInRegisterResponse register() {
        HouseConfiguration houseConfig = new HouseConfiguration();
        houseConfig.setHouseId("houseID123");
        houseConfig.setSensors(new ArrayList<SensorDevice>());

        UserPreference userPref = new UserPreference();
        userPref.setFirstName("Samantha");
        userPref.setLastName("Smith");
        userPref.setUserId(2);
        userPref.setPriority(2);
        userPref.heatingPreference = new HeatingPreference();
        userPref.heatingPreference.setTargetTemp(23);
        userPref.heatingPreference.setAutomationType("location");
        userPref.lightingPreference = new LightingPreference();
        userPref.lightingPreference.setBrightness(200);
        userPref.lightingPreference.setSaturation(200);
        userPref.lightingPreference.setAutomationType("light");
        userPref.lightingPreference.setColour(new CustomLightColour(50, 230, 10));

        return new LogInRegisterResponse(houseConfig, userPref);
    }
}
